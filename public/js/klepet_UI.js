/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  var res = vhodnoBesedilo.split("http");
  for (var i in res) {
      if (res[i].match(/.*\.(png|jpg|gif)/gi) != null) {
          vhodnoBesedilo += "<br /><img style='margin-left:20px; width:200px;' src='http" + res[i].match(/.*\.(png|jpg|gif)/gi) + "' />";
      }
      
  }
  return vhodnoBesedilo;
 /*var slike = vhodnoBesedilo.match(/https?:\/\/.*\.(png|jpg|gif)/gi);
 if (slike.length > 0) {
   for (var slika in slike) {
    vhodnoBesedilo = vhodnoBesedilo + "<img src='" + slika +"'/>";
    console.log(vhodnoBesedilo);
   }
 }
 console.log(vhodnoBesedilo);
 return vhodnoBesedilo;*/
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jePoke = sporocilo.indexOf('&#9756;') > -1;
  //var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = sporocilo.indexOf("http" > -1);
  if (jeSlika) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;br /&gt;").join("<br />")
               .split("&lt;img").join("<img")
               .split(".png' /&gt;").join(".png />")
               .split(".jpg' /&gt;").join(".jpg' />")
               .split(".gif' /&gt;").join(".gif' />");
    //console.log(sporocilo);
    return divElementHtmlTekst(sporocilo);
  }
  else if (jePoke) {
    return divElementHtmlTekst(sporocilo);
  }

  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function preimenuj(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  besede.shift();
  var besedilo = besede.join(' ');
  var parametri = besedilo.split('\"');
  if (parametri) {
    var uporabnik = parametri[1];
    var nadimek = parametri[3];
    if (vsiUporabniki.indexOf(uporabnik) > -1 & uporabnik != trenutniVzdevek) {
      if (uporabnikGledeNaNadimek.indexOf(uporabnik) > -1) {
        nadimki[uporabnikGledeNaNadimek.indexOf(uporabnik)] = nadimek;
      } else {
        uporabnikGledeNaNadimek.push(uporabnik);
        nadimki.push(nadimek);
      }
      //console.log(uporabnikNadimek);
      //console.log(nadimki, uporabnikGledeNaNadimek);
      sporocilo = "Uporabnik " + uporabnik +" je dobil nadimek " + nadimek;
    } else {
      sporocilo = "Uporabnika ni bilo mogoče preimenovati.";
    }
  } else {
    sporocilo = 'Neznan ukaz';
  }
  return sporocilo;
}

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  
  
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    if (sporocilo.match(/\/preimenuj/)) {
      sistemskoSporocilo = preimenuj(sporocilo);
      } 
    else {
      sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo,uporabnikGledeNaNadimek,nadimki);
    }
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
    
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}




var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var uporabnikGledeNaNadimek = [];
var nadimki = [];
var vsiUporabniki = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    /*console.log(sporocilo);
    var parametri = sporocilo.besedilo.split(":");
    console.log(parametri);
    var ime = parametri[0];
    console.log(ime);
    if (uporabnikGledeNaNadimek.indexOf(ime) > -1) {
      sporocilo = uporabnikGledeNaNadimek.indexOf(ime) + ":";
      for (var i = 1; i < parametri.length; i++) {
        sporocilo +=  parametri[i];
      }
    }*/
    for (var i = 0; i < uporabnikGledeNaNadimek.length; i++) {
      var re = new RegExp(uporabnikGledeNaNadimek[i]);
      
      if (sporocilo.besedilo.match(re) != null) {
        //console.log(re);
        
        
        sporocilo.besedilo = sporocilo.besedilo.split(uporabnikGledeNaNadimek[i]).join(nadimki[i] + " (" + uporabnikGledeNaNadimek[i] + ") ");
        
        if (sporocilo.besedilo.match("se je preimenoval v") != null) {
          var parametri = sporocilo.besedilo.split(" se je preimenoval v ");
          uporabnikGledeNaNadimek[i] = (parametri[1]).slice(0, -1);
          //preimenuj('preimenovanje', parametri[1], nadimki[i]);
          console.log(uporabnikGledeNaNadimek,nadimki);
        } 
        //console.log(sporocilo.besedilo);
      }
      //sporocilo.besedilo.replace(re, nadimki[i]);
    }
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    vsiUporabniki = [];
    for (var i=0; i < uporabniki.length; i++) {
      vsiUporabniki.push(uporabniki[i]);
    }
    for (var i=0; i < uporabniki.length; i++) {
      var indeks = uporabnikGledeNaNadimek.indexOf(uporabniki[i]);
      if (indeks > -1) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[indeks] + " (" + uporabniki[i] +")"));
      } else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    
     // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-uporabnikov div').click(function() {
      var sporocilo;
      //$('#sporocila').append(divElementEnostavniTekst('(zasebno za ' + $(this.text()) + '): &#9756;');
      var ime = $(this).text();
      if (ime.indexOf("(") > -1 & ime.indexOf(")") > -1) {
        ime = ime.substring(ime.indexOf("(") + 1,ime.indexOf(")"));
      }
      if (ime != trenutniVzdevek) {
        sporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"' + ime + '"' + ' "' + '&#9756;' + '"',uporabnikGledeNaNadimek,nadimki);
      } else {
        sporocilo = "Sporočila uporabniku z vzdevkom " + trenutniVzdevek + " ni bilo mogoče poslati.";
      }
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


